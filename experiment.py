#!/usr/bin/python3

import execo_engine
import random
import statistics
import platform
import sys

from experiment_tools import *

class ProfilingEngine(execo_engine.Engine):
    def __init__(self):
        super(ProfilingEngine, self).__init__()
        self.args_parser.add_argument('--target', default=''),

    def start_bench(self, params):

        workload = 'stress-ng -c 0 -t 10'
        #workload = 'sleep .5'
        
        monitoring=' -s -f '+str(params['freq'])+' -t 0'

        nb = params['nb']
        if nb == 0:
            monitoring = ' -n'+monitoring
        else:
            counters = random.sample(self.pmt, nb)
            if not self.pmt[0] in counters:
                counters[0] = self.pmt[0]
            counters_txt = ','.join(sorted(counters))
            monitoring = ' -p '+counters_txt+monitoring

        if not params['rapl']:
            monitoring = monitoring+' -r'

        w_out, mon_out, total_time = start_bench(workload, monitoring, stop_with_bench=True)

        rapl_mode = 'rapl_true' if params['rapl'] else 'rapl_false'

        perf_data=extract_monitoring(mon_out,statistics.median  ,-1)
        precision_data=extract_monitoring(mon_out,sum ,(1,-1))

        fields_val, fields_name='None', 'None'
        if precision_data != []:
            fields_val=','.join([str(d) for d in precision_data])
            fields_name = ','.join(extract_monitoring_title(mon_out,(1,-1)))
        
        res = str(nb)+' '+str(total_time)+' '+str(extract_monitoring_duration(mon_out))+' ' \
              +str(perf_data)+' ' \
              +str(params['freq'])+' '+fields_val+' ' \
              +fields_name+' '+rapl_mode+' '+platform.node()
        dump_raw(self.result_file+'_'+platform.node(), res)
        return True

    def generic_expe(self, key_name, pmt, rapl=[False], zero=False):
        print(' Reference Directory : ', key_name)
        self.result_file = key_name+'_res_file'
        self.pmt = pmt
        
        params = {"nb": range(0 if zero else 1, len(self.pmt)+1),
                  "freq": [1,4,16,64,128],
                  "repeat": range(10),"rapl":rapl}
        #params = {"nb": range(min(len(self.pmt)+1, 3)), "freq": [10, 20, 30], "repeat": range(2), "rapl":[True, False]}
        #params = {"nb": range(3), "freq": [10, 20, 30], "repeat": range(2), "rapl":[False, True]}
        execute(self, params, key_name)
    
    def run(self):
        (hw,sw,hw_cache) = get_counters()
        directory = '' if self.args.target == '' else self.args.target+'/'
        
        self.generic_expe(directory+'output_precision_hw_all', hw+hw_cache, [True, False])
        self.generic_expe(directory+'output_precision_hw_sw', [hw[0], sw[0]], [True, False], zero=True)
        self.generic_expe(directory+'output_precision_hw_sw_2', [sw[0]], [True, False])
        self.generic_expe(directory+'output_precision_hw', hw)
        self.generic_expe(directory+'output_precision_hw_cache', hw_cache)
        self.generic_expe(directory+'output_precision_sw', sw)

if __name__ == "__main__":
    engine = ProfilingEngine()
    engine.start()
