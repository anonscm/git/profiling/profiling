#!/usr/bin/python3

import execo_engine
import random
import statistics
import platform
import sys

from experiment_tools import *

class ProfilingEngine(execo_engine.Engine):
    def __init__(self):
        super(ProfilingEngine, self).__init__()
        self.args_parser.add_argument('--target', default=''),

    def start_bench(self, params):

        workload = 'stress-ng -c 0 -t 500'
            
        nb = params['nb']
        counters = random.sample(self.pmt, nb)
        counters_txt = ','.join(counters)
        monitoring=' -s -f '+str(params['freq'])+' -t 10'
        if params['rapl']:
            monitoring = monitoring+' -r'
        if nb == 0:
            monitoring = ' -n'+monitoring
        else:
            monitoring = ' -p '+counters_txt+monitoring
        
        w_out, mon_out, total_time = start_bench(workload, monitoring, stop_with_bench=False)

        data=extract_monitoring(mon_out,statistics.median  ,-1)
        rapl_mode = 'rapl_true' if params['rapl'] else 'rapl_false'
        res = str(nb)+' '+str(params['freq'])+' '+str(data)+' '+counters_txt+' '+rapl_mode+' '+platform.node()
        dump_raw(self.result_file+'_'+platform.node(), res)
        return True
    
    def generic_expe(self, key_name, pmt, rapl=['False']):
        print(' Reference Directory : ', key_name)
        self.result_file = key_name+'_res_file'
        self.pmt = pmt
        params = {"nb": range(len(self.pmt)+1),
                  "freq": [1,4,16,64,128],
                  "repeat": range(20),
                  "rapl": rapl}
        execute(self, params, key_name)

    def run(self):
        (hw,sw,hw_cache) = get_counters()
        directory = '' if self.args.target == '' else self.args.target+'/'

        self.generic_expe(directory+'output_overhead_hw_all', hw+hw_cache, [True, False])
        self.generic_expe(directory+'output_overhead_hw_sw', [hw[0], sw[0]], [True, False])
        self.generic_expe(directory+'output_overhead_hw', hw)
        self.generic_expe(directory+'output_overhead_hw_cache', hw_cache)
        self.generic_expe(directory+'output_overhead_sw', sw)
        
if __name__ == "__main__":
    engine = ProfilingEngine()
    engine.start()
