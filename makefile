objects = system_profiling system_profiling_nn
all: $(objects)



system_profiling_group:system_profiling.c counters_group.o counters.h rapl.o rapl.h network.h network.o counters_option.h
	gcc $(DEBUG) -O3 -Wall -o system_profiling_group system_profiling.c counters_group.o rapl.o network.o -lpowercap  -L"/home/mlandman/lib64" -I"/home/mlandman/include"

system_profiling_group_nn:system_profiling_nn.c counters_group.o counters.h rapl.o rapl.h network.h network.o counters_option.h
	gcc $(DEBUG) -O3 -Wall -o system_profiling_group_nn system_profiling_nn.c counters_group.o rapl.o network.o -lpowercap  -L"/home/mlandman/lib64" -I"/home/mlandman/include"

system_profiling:system_profiling.c counters_individual.o counters.h rapl.o rapl.h network.h network.o counters_option.h
	gcc $(DEBUG) -O3 -Wall -o system_profiling system_profiling.c counters_individual.o rapl.o network.o -lpowercap  -L"/home/mlandman/lib64" -I"/home/mlandman/include"

system_profiling_nn:system_profiling_nn.c counters_individual.o counters.h rapl.o rapl.h network.h network.o counters_option.h
	gcc $(DEBUG) -O3 -Wall -o system_profiling_nn system_profiling_nn.c counters_individual.o rapl.o network.o -lpowercap  -L"/home/mlandman/lib64" -I"/home/mlandman/include"

counters_option.h: counters_option.py
	./counters_option.py > counters_option.h

debug: DEBUG = -DDEBUG

debug: all

rapl.o: rapl.c rapl.h
	gcc -O3 -Wall -c rapl.c -I"/home/mlandman/include"

network.o: network.c network.h
	gcc -Wall -c network.c

counters_individual.o:counters_individual.c counters.h
	gcc -O3 -Wall -c counters_individual.c

counters_group.o:counters_group.c counters.h
	gcc -O3 -Wall -c counters_group.c

clean:
	\rm -f *~ *.o system_profiling_group system_profiling counters_option.h system_profiling_group_nn system_profiling_nn
