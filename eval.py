  #pour l'__init__

'''
/sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed

savoir le nombre de coeur
ls -l /sys/devices/system/cpu/ | grep -c "cpu*[0-9]"
ls -l /sys/devices/system/cpu/cpufreq/ | grep -c "policy*[0-9]" (pour grid500)

/!\ sur grid 500
/sys/devices/system/cpu/cpufreq/policy6/scaling_setspeed

sudo-g5k echo 1200000 > /sys/devices/system/cpu/cpufreq/policy6/scaling_setspeed

changer freq
echo 700000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed




'''

import sys
import argparse
import os
import time
import torch
import fc_model as md


def load_checkpoint(filepath):
    chem = os.path.dirname(os.path.realpath(__file__))
    #print("dir= ",chem)
    filepath =chem + "/"+filepath
    checkpoint = torch.load(filepath)
    model = md.Net(checkpoint['input_size'],checkpoint['output_size']) #rajouter une entree dans le dico hidden layer WARNING !!!!
    model.load_state_dict(checkpoint['state_dict'])

    return model


startnn = time.time()


parser = argparse.ArgumentParser(description='This is a python script to eval with a model.')
parser.add_argument('-n',"--name", type=str, help='le nom du modele a utiliser')
parser.add_argument('-nbc',"--nbcoeur", type=str, help='le nombre de coeur de la machine')
parser.add_argument('-arg',"--argument", type=str, help='les parametres d\'entrées séparés par des virgules')

args = parser.parse_args()

input =  list(map(float, args.argument.split(",")))
input = torch.Tensor(input)
input =input.view(1,-1)

model = load_checkpoint(args.name)
model.eval()

with torch.no_grad():
    res= model(input)
    res = torch.exp (res)

# 0 : performance   1 :powersave
#freq = "performance" if res[0] > res[1] else "powersave" #si appel shell

nbcoeur= int(args.nbcoeur)
res = res.view(-1)
#print("res = ",res,"size", res.size()) ##suppp

endnn = time.time()

startcpu = time.time()

if res[0] > res[1] :
    cpufreq="for i in `seq 0 "+str(nbcoeur)+"`; do sudo cpufreq-set -c $i -g performance; done"
    os.system(cpufreq)

else:
    cpufreq="for i in `seq 0 "+ str(nbcoeur )+"`; do sudo cpufreq-set -c $i -g powersave; done"

    os.system(cpufreq)

endcpu = time.time()
'''
startecho = time.time()
#AVEC SCALING SETSPEED -> PERMISSION DENIED
for i in range (nbcoeur):
    chem = "/sys/devices/system/cpu/cpufreq/policy" + str(i)

    if res[0] > res[1] :
        cat = "sudo cat " + chem +"/scaling_max_freq"
        #print(cat)

        maxfreq = os.popen(cat).read()
        maxfreq = maxfreq.rstrip()
        #print("freq max de policy",i," = ",maxfreq)
        echo = "sudo echo "+ maxfreq +" | sudo tee " + chem +"/scaling_setspeed"
        #print(echo)
        os.system(echo)
    else:
        cat = "sudo cat " + chem + "/scaling_min_freq"
        #print(cat)

        minfreq = os.popen(cat).read()
        minfreq = minfreq.rstrip()
        #print("freq min de policy", i, " = ", minfreq)

        echo = "sudo  echo " + minfreq + " | sudo tee " + chem + "/scaling_setspeed"
        #print(echo)
        os.system(echo)

for i in range (nbcoeur):
    chem = "/sys/devices/system/cpu/cpufreq/policy" + str(i)

    if res[0] > res[1] :
        cat = "sudo cat " + chem +"/scaling_max_freq"
        #print(cat)

        maxfreq = os.popen(cat).read()
        maxfreq = maxfreq.rstrip()
        #print("freq max de policy",i," = ",maxfreq)
        echo = " echo performance | sudo tee " + chem + "/scaling_governor"
        #print(echo)
        os.system(echo)
    else:
        cat = "sudo cat " + chem + "/scaling_min_freq"
        #print(cat)

        minfreq = os.popen(cat).read()
        minfreq = minfreq.rstrip()
        #print("freq min de policy", i, " = ", minfreq)

        echo = " echo powersave | sudo tee " + chem + "/scaling_governor"
        #print(echo)
        os.system(echo)
'''


#print("temps nn + cpufreq-set= ", (endnn - startnn) + (endcpu-startcpu))
#print("\n temps nn + echo dans fich systeme= ", (endnn - startnn) + (endecho-startecho))
