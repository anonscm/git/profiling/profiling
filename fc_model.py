import torch
from torch import nn
import torch.nn.functional as F
from torch.autograd import Variable
import matplotlib.pyplot as plt


class Net(nn.Module):
    def __init__(self, input_size, output_size,hidden_size = 3, drop_p = 0.2):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.fc2 = nn.Linear(hidden_size,output_size)
        self.dropout = nn.Dropout(p=drop_p)
        self.out_act = nn.LogSoftmax(dim =1) #donne une proba entre 0 et 1
    def forward(self, input_):
        x = F.relu(self.fc1(input_))
        x = self.dropout(x)
        x = self.fc2(x)
        x = self.out_act(x)
        return x

def train_model(model, optimizer, criterion, l_input, l_output, nb_epoch, l_valid_inp, l_valid_out):
    train_losses, test_losses = [], []
    model.train()
    iteration_number = 51
    l_input = Variable(l_input)
    l_output =Variable(l_output)
    for iteration in range(nb_epoch):
        ## TRAINING
        # optimization
        optimizer.zero_grad() #Clears the gradients of all optimized torch.Tensor

        # Forward to get output
        results = model.forward(l_input)

        # Calculate Loss
        loss = criterion(results, l_output)

        # backward propagation
        loss.backward()

        # Updating parameters
        optimizer.step()

        # store loss
        train_losses.append(loss.data)


        ## VALIDATION TEST
        test_loss = 0
        with torch.no_grad():
            equals= torch.tensor([], dtype = torch.uint8)
            res=model(l_valid_inp)
            lossv = criterion(res, l_valid_out)
            ps = torch.exp(res)
            top_p, top_class = ps.topk(1,dim=1)
            eq = top_class == l_valid_out.view(*top_class.shape)
            equals = torch.cat((equals,eq),0)


            #store loss
            test_losses.append(lossv.data)
            accuracy = torch.mean( equals.type(torch.FloatTensor))

        print("Epoch {}/{}   ".format(iteration+1, nb_epoch),
        'Training loss: {:.3f},  Validation loss: {:.3f},  '.format( train_losses[iteration],test_losses[iteration]),
        'Accuracy: {:.2f}%'.format(accuracy.item()*100))

    plt.plot(test_losses, label = 'Validation Loss')
    plt.plot(train_losses, label = 'training Loss')
    #plt.plot(range(iteration_number),train_losses)
    plt.xlabel("Number of Iterations")
    plt.ylabel("Loss")
    plt.legend()
    plt.show()
