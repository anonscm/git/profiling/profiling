#! /bin/bash

cd ~/profiling

git pull

sudo-g5k sh -c 'echo 0 >/proc/sys/kernel/perf_event_paranoid'
sudo chmod a+w /sys/class/powercap/intel-rapl/*/*
sudo chmod a+w /sys/class/powercap/intel-rapl/*/*/*
sudo-g5k dpkg -i grid5000/*deb


make
pip3 install --user execo
sudo apt -y install stress-ng

./experiment.py --target "$1"

# to run :

# oarsub -t besteffort -p "cluster='nova'" -l walltime=4:0:0 ~/profiling/run_experiment.sh
