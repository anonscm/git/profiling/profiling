#! /usr/bin/python3
import execo
import execo_engine
import time

def treat_monitoring(output):
    lines= output.strip().split('\n')
    data = [[float(e) for e in line.split()] for line in lines[1:]]
    names = lines[0].split()[1:]
    
    aggreggated = [sum(e) for e in zip(*data)]
        
    res={}
    for name,val in zip(names,aggreggated[1:]):
        res[name]=val
    return(res)

def extract_monitoring(output, function, column):
    lines= output.strip().split('\n')
    if type(column)==int:
        data = [float(line.split()[column]) for line in lines[1:]]
        return(function(data))
    else:
        a,b = column

        data = [[float(e) for e in line.split()[a:b]] for line in lines[1:]]
        res = [function(sublist) for sublist in zip(*data)]

        return res

def extract_monitoring_title(output, column):
    line = output.strip().split('\n')[0].split()
    if type(column)==int:
        return line[column]
    else:
        (a,b)=column
        return line[a:b]

def extract_monitoring_duration(output):
    a = float(output.strip().split('\n')[1].split()[0])
    b = float(output.strip().split('\n')[-1].split()[0])
    return b-a

def get_counters():
    with open('counters_option.h') as file:
        content = file.readlines()
        hw = [ line.replace('"',' ').split()[3] for line in content
               if 'HARDWARE' in line ]
        sw = [ line.replace('"',' ').split()[3] for line in content
               if 'SOFTWARE' in line ]
        hw_cache = [ line.replace('"',' ').split()[3] for line in content
                     if 'HW_CACHE' in line ]
        return(hw, sw, hw_cache)

def dump_raw(result_file, data):
    with open(result_file, "a") as f:
        f.write(data+'\n')

import tempfile
import os
def start_bench(workload_cmd, profiling_params, stop_with_bench=True):
    
    workload = execo.Process(workload_cmd)

    if stop_with_bench:
        (handler,fname) = tempfile.mkstemp()
        os.close(handler)

        monitoring=execo.Process('./system_profiling -o "'+fname+'" '+profiling_params)
        ref = time.time()
        with monitoring.start():
            workload.run()
            monitoring.kill(sig=15, auto_force_kill_timeout=False)
        delta = time.time()-ref
        with open(fname) as f:
            mon = f.read()
        return workload.stdout, mon, delta

    else:
        monitoring=execo.Process('./system_profiling '+profiling_params)
        ref = time.time()
        with workload.start():
            monitoring.run()
            workload.kill()
        delta = time.time()-ref

        return workload.stdout, monitoring.stdout, delta

def execute(engine, params, key_name):
    combs = execo_engine.sweep(params)
    sweeper = execo_engine.ParamSweeper(key_name, combs)
    for comb in sweeper.get_remaining():
        print(key_name, comb)
        if(engine.start_bench(comb)):
            sweeper.done(comb)
        else:
            sweeper.skip(comb)
